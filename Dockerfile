FROM maven as build
WORKDIR /app
COPY . .
RUN mvn -s ci_settings.xml clean  install -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.test.skip=true


FROM openjdk:8-jre-slim
COPY --from=build /app/target/*.jar /usr/local/lib/app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]

