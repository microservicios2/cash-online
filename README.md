# Examen Backend Cash

## About
Proyecto "Backend Cash" desarrollado con springboot 2.3.3, utilizando JPA repository y base de datos H2.

### Pre-requisitos 📋

- JDK 1.8

### Instalación 🔧

- Bajar dependencias necesarias para el proyecto

```
mvn clean install -U
```

### H2 Database Administration

- GUI para administrar DB en memoria [https://localhost:8080/h2-console](https://localhost:8080/h2-console)

```
datasource: jdbc:h2:mem:test
username: cashonline
password: cashonline
```

### Documentacion
- Swagger [https://localhost:8080/swagger-ui.html](https://localhost:8080/swagger-ui.html/)

### Autor

* **Ignacio Lopez** - [https://lignacio.ar](https://lignacio.ar/)
