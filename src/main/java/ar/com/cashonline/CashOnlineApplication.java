package ar.com.cashonline;

import ar.com.cashonline.model.Loan;
import ar.com.cashonline.model.User;
import ar.com.cashonline.repository.LoanRepository;
import ar.com.cashonline.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


@EnableJpaRepositories
@SpringBootApplication
public class CashOnlineApplication {

    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(CashOnlineApplication.class, args);
    }

    @PostConstruct
    void loadData() {
        Loan loan1 = new Loan();
        loan1.setTotal(55669.99f);

        Loan loan2 = new Loan();
        loan2.setTotal(10900.50f);

        List<Loan> loans = new ArrayList<>();
        loans.add(loan1);
        loans.add(loan2);

        User user = new User();
        user.setEmail("igloar96@gmail.com");
        user.setFirstName("ignacio");
        user.setLastName("Lopez");
        user.setLoans(loans);

        this.userRepository.save(user);
    }
}
