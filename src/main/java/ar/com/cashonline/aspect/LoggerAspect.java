package ar.com.cashonline.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggerAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerAspect.class);
/*
* Logger automatico a nivel de servicios
* */
    @Around("execution( * ar.com.cashonline.service.*.*(..))")
    public Object logs(ProceedingJoinPoint joinPoint) throws Throwable {
        ar.com.cashonline.aspect.LoggerAspect.LOGGER.info(String.format("Cash-Online %s %s %s", joinPoint.getKind(),
                joinPoint.getTarget().getClass(),
                joinPoint.getSignature().getName()));
        try {
            return joinPoint.proceed();
        } catch (Exception e) {
            ar.com.cashonline.aspect.LoggerAspect.LOGGER.error(e.getMessage());
            throw e;
        }

    }


}
