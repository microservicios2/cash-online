package ar.com.cashonline.config;

import ar.com.cashonline.model.exceptions.DuplicadoException;
import ar.com.cashonline.model.exceptions.NoEncontradoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.UnexpectedTypeException;


@ControllerAdvice
class GlobalControllerExceptionHandler {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleMsg(HttpMessageNotReadableException ex) {
        return new ResponseEntity<String>("Body mal formado", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleBadRequests1(MethodArgumentNotValidException ex) {
        return new ResponseEntity<String>(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnexpectedTypeException.class)
    public ResponseEntity<String> handleBadRequests0(UnexpectedTypeException ex) {
        return new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<String> handleBadRequests(BindException ex) {
        return new ResponseEntity<String>(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoEncontradoException.class)
    public ResponseEntity<String> handleNotFound(NoEncontradoException ex) {
        return new ResponseEntity<String>("Objeto no encontrado.",HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicadoException.class)
    public ResponseEntity<String> handleDuplicated(DuplicadoException ex) {
        return new ResponseEntity<String>("Ya existe un objeto similar.",HttpStatus.CONFLICT);
    }

}