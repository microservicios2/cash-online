package ar.com.cashonline.controller;

import ar.com.cashonline.model.Loan;
import ar.com.cashonline.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoansController {

    @Autowired
    private LoanService loanService;

    @GetMapping("/loans")
    public Page<Loan> getAll(
            @RequestParam(name="page") String page,
            @RequestParam(name="size") String size) throws Exception {
        Pageable pageable = PageRequest.of(Integer.parseInt(page)-1, Integer.parseInt(size));

        return this.loanService.getLoans(pageable);
    }

    @GetMapping(value = "/loans",params = "user_id")
    public Page<Loan> getAllWithId(
            @RequestParam(name="user_id") String userId,
            @RequestParam(name="page") String page,
            @RequestParam(name="size") String size) throws Exception {
        Pageable pageable = PageRequest.of(Integer.parseInt(page)-1, Integer.parseInt(size));

            return this.loanService.getLoansById(userId, pageable);

    }


}
