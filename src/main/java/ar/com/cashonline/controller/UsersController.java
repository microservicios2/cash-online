package ar.com.cashonline.controller;

import ar.com.cashonline.model.dto.request.UserRequestDto;
import ar.com.cashonline.model.dto.response.UserResponseDto;
import ar.com.cashonline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;


    @GetMapping("/{id}")
    public UserResponseDto identificar(
            @Valid @PathVariable String id) throws Exception {
        return this.userService.findUserById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(
            @Valid @PathVariable String id) throws Exception {
        this.userService.deleteUser(id);
    }

    @PostMapping
    public UserResponseDto crearUsuario(
            @Valid @RequestBody UserRequestDto userRequestDto) throws Exception {
        return this.userService.createUser(userRequestDto);
    }

}
