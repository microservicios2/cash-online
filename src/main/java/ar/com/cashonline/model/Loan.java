package ar.com.cashonline.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Loan {
    @Id()
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Float total;
    @Column(name = "USER_ID")
    private Long userId;

}
