package ar.com.cashonline.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    @JoinColumn(name = "USER_ID")
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Loan> loans;
}
