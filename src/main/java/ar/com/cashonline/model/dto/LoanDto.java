package ar.com.cashonline.model.dto;

import ar.com.cashonline.model.Loan;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoanDto {
    private Long id;
    private Float total;
    private Long userId;

    public LoanDto(Loan loan ) {
        this.id = loan.getId();
        this.total = loan.getTotal();

    }
}
