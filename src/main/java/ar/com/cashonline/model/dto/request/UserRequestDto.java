package ar.com.cashonline.model.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class UserRequestDto {
    private Long id;
    @NotEmpty(message = "El campo email es obligatorio")
    private String email;
    @NotEmpty(message = "El campo firstName es obligatorio")
    private String firstName;
    @NotEmpty(message = "El campo lastName es obligatorio")
    private String lastName;
}
