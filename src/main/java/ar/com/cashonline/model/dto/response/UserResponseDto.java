package ar.com.cashonline.model.dto.response;

import ar.com.cashonline.model.Loan;
import ar.com.cashonline.model.dto.LoanDto;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class UserResponseDto {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private List<Loan> loans=new ArrayList<>();
}