package ar.com.cashonline.model.exceptions;

public class DuplicadoException extends Exception{
    public DuplicadoException() {
        super();
    }

    public DuplicadoException(String message) {
        super(message);
    }

    public DuplicadoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicadoException(Throwable cause) {
        super(cause);
    }

    protected DuplicadoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
