package ar.com.cashonline.model.exceptions;

public class NoEncontradoException extends Exception{
    public NoEncontradoException() {
    }

    public NoEncontradoException(String message) {
        super(message);
    }

    public NoEncontradoException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoEncontradoException(Throwable cause) {
        super(cause);
    }

    public NoEncontradoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
