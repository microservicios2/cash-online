package ar.com.cashonline.service;

import ar.com.cashonline.model.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LoanService {
    Page<Loan> getLoans(Pageable pageable);
    Page<Loan> getLoansById(String userId, Pageable pageable);
}
