package ar.com.cashonline.service;

import ar.com.cashonline.model.Loan;
import ar.com.cashonline.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanRepository loanRepository;

    @Override
    public Page<Loan> getLoans(Pageable pageable) {
        return this.loanRepository.findAll(pageable);
    }

    @Override
    public Page<Loan> getLoansById(String userId, Pageable pageable) {
        return this.loanRepository.findByUserId(Long.parseLong(userId),pageable);
    }
}
