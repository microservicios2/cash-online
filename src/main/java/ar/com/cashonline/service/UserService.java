package ar.com.cashonline.service;

import ar.com.cashonline.model.dto.request.UserRequestDto;
import ar.com.cashonline.model.dto.response.UserResponseDto;
import ar.com.cashonline.model.exceptions.DuplicadoException;
import ar.com.cashonline.model.exceptions.NoEncontradoException;

import java.util.List;

public interface UserService {
    UserResponseDto findUserById(String userId) throws NoEncontradoException;
    UserResponseDto createUser(UserRequestDto userId) throws DuplicadoException;
    void deleteUser(String userId) throws NoEncontradoException;
    List<UserResponseDto> findAll();
}
