package ar.com.cashonline.service;

import ar.com.cashonline.model.User;
import ar.com.cashonline.model.dto.request.UserRequestDto;
import ar.com.cashonline.model.dto.response.UserResponseDto;
import ar.com.cashonline.model.exceptions.DuplicadoException;
import ar.com.cashonline.model.exceptions.NoEncontradoException;
import ar.com.cashonline.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserResponseDto findUserById(String userId) throws NoEncontradoException {
        User user = this.userRepository.findById(Long.parseLong(userId)).orElseThrow(() -> new NoEncontradoException());
        return userToUserDto(user);
    }


    @Override
    public UserResponseDto createUser(UserRequestDto userDto) throws DuplicadoException {
        User user = this.userRepository.save(this.userDtoToUser(userDto));
        return userToUserDto(user);
    }

    @Override
    public void deleteUser(String userId) throws NoEncontradoException {
        this.userRepository.deleteById(Long.parseLong(userId));
    }

    @Override
    public List<UserResponseDto> findAll() {
        throw new NotImplementedException();
    }


    private UserResponseDto userToUserDto(User user) {
        UserResponseDto udto = new UserResponseDto();
        udto.setEmail(user.getEmail());
        udto.setFirstName(user.getFirstName());
        udto.setId(user.getId());
        udto.setLastName(user.getLastName());
        udto.setLoans(user.getLoans());
        return udto;
    }

    private User userDtoToUser(UserRequestDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setLastName(userDto.getLastName());
        user.setFirstName(userDto.getFirstName());
        user.setEmail(userDto.getEmail());
        return user;
    }

}
